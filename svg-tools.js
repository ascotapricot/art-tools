"use strict";

/* ***************************************

Art Tools

Copyright (C) 2018 Ascot Apricot
https://gitlab.com/ascotapricot
ascotapricot@gmail.com

This file is a part of Art Tools.

You should have received a copy of the GNU General Public License
along with Art Tools.  If not, see <https://www.gnu.org/licenses/>

*************************************** */

/* exported clear_canvas */

// Some basic drawing utilities

function clear_canvas(canvas) {
	while (canvas.firstChild) {
		canvas.removeChild(canvas.firstChild);
	}
}
