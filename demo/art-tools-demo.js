"use strict";

/* ***************************************

Art Tools

Copyright (C) 2018 Ascot Apricot
https://gitlab.com/ascotapricot
ascotapricot@gmail.com

This file is a part of Art Tools.

Art Tools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Art Tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Art Tools.  If not, see <https://www.gnu.org/licenses/>

*************************************** */

function draw() {

	var drawing_group = document.getElementById("drawing_group");

	clear_canvas(drawing_group);

	// Get control values
	var sketch_slider = document.getElementById("sketch_slider").value*10;
	var pressure_slider = document.getElementById("pressure_slider").value*1;
	var hardness_slider = document.getElementById("hardness_slider").value*1;
	var sharpness_slider = document.getElementById("sharpness_slider").value*1;
	var fill_slider = document.getElementById("fill_slider").value*1;
	var shading_choice = document.getElementById("shading_choice").value;
	var color_slider = document.getElementById("color_slider").value;
	var color_check = document.getElementById("color_check").checked;

	// New Pencil object to draw with
	var hue;
	if (color_check) {
		hue = color_slider;
	} else {
		hue = -2;
	}

	var pencil = new AT_Pencil(drawing_group, hardness_slider, sharpness_slider, hue);

	// Draw some basic lines
	var line_1 = new AT_Line(new AT_Point(50,125), new AT_Point(200,125));
	var line_2 = new AT_Line(new AT_Point(50,50), new AT_Point(200,200));
	var line_3 = new AT_Line(new AT_Point(50,200), new AT_Point(200,50));
	var line_4 = new AT_Line(new AT_Point(125,50), new AT_Point(125,200));
	pencil.line_straight(line_1, sketch_slider, pressure_slider, true);
	pencil.line_straight(line_2, sketch_slider, pressure_slider, true);
	pencil.line_straight(line_3, sketch_slider, pressure_slider, true);
	pencil.line_straight(line_4, sketch_slider, pressure_slider, true);

	var rect_1b = new AT_Rect(new AT_Point(250, 50), 150, 150);
	pencil.hatching_rectangle(rect_1b, shading_choice[0] , shading_choice[1], fill_slider, sketch_slider, pressure_slider);

	var rect_1c = new AT_Rect(new AT_Point(450, 50), 150, 150);
	pencil.crosshatching_rectangle(rect_1c, shading_choice[0] , shading_choice[1], fill_slider, sketch_slider, pressure_slider);

	var rect_1d = new AT_Rect(new AT_Point(650, 50), 150, 150);
	pencil.stipple_rectangle(rect_1d, shading_choice[0] , shading_choice[1], fill_slider, sketch_slider, pressure_slider);

	var circle_2a = new AT_Circle(new AT_Point(125,325), 75);
	pencil.line_circle(circle_2a, [1,2,3,4] , sketch_slider, pressure_slider );

	var circle_2b = new AT_Circle(new AT_Point(325,325), 75);
	pencil.hatching_circle(circle_2b, shading_choice[0], shading_choice[1], fill_slider, sketch_slider, pressure_slider);

	var pts_array = [];
	pts_array.push(new AT_Point(450, 250));
	pts_array.push(new AT_Point(600, 250));
	pts_array.push(new AT_Point(450, 325));
	pts_array.push(new AT_Point(600, 400));
	var polyline_2c = new AT_Polyline(pts_array);
	var i;
	for (i = 0; i < polyline_2c.segments.length; i++) {
		pencil.line_straight(polyline_2c.segments[i], sketch_slider, pressure_slider, true);
	}

	var rect_3a1 = new AT_Rect(new AT_Point(50, 450), 150, 50);
	pencil.hatching_rectangle(rect_3a1, shading_choice[0] , shading_choice[1], fill_slider, sketch_slider, pressure_slider);

	var rect_3a2 = new AT_Rect(new AT_Point(50, 525), 150, 50);
	pencil.crosshatching_rectangle(rect_3a2, shading_choice[0] , shading_choice[1], fill_slider, sketch_slider, pressure_slider);

	var rect_3b1 = new AT_Rect(new AT_Point(250, 450), 50, 200);
	pencil.hatching_rectangle(rect_3b1, shading_choice[0] , shading_choice[1], fill_slider, sketch_slider, pressure_slider);

	var rect_3b2 = new AT_Rect(new AT_Point(325, 450), 50, 200);
	pencil.crosshatching_rectangle(rect_3b2, shading_choice[0] , shading_choice[1], fill_slider, sketch_slider, pressure_slider);

}


function reset() {
	var sketch_slider = document.getElementById("sketch_slider");
	var pressure_slider = document.getElementById("pressure_slider");
	var hardness_slider = document.getElementById("hardness_slider");
	var sharpness_slider = document.getElementById("sharpness_slider");
	var fill_slider = document.getElementById("fill_slider");
	var shading_choice = document.getElementById("shading_choice");
	var color_slider = document.getElementById("color_slider");
	var color_check = document.getElementById("color_check");

	sketch_slider.value = 0.5;
	pressure_slider.value = 0.5;
	hardness_slider.value = 0.5;
	sharpness_slider.value = 0.5;
	fill_slider.value = 0.5;
	color_slider.value = 0;
	shading_choice.value = "00";
	color_check.checked = false;

	draw();

}

function add_slider(parent, label_text, min, max, step, value, id) {

	// Main Container
	// 		Label
	//		Wrapper
	//			Input

	// Main Container
	var input_container = document.createElement("div");
	input_container.setAttribute("class", "input_container");
	parent.appendChild(input_container);

	// Input Label
	var input_label = document.createElement("div");
	input_label.setAttribute("class", "input_label");
	input_label.innerHTML = label_text;
	input_container.appendChild(input_label);

	// Input Wrapper
	var input_wrapper = document.createElement("div");
	input_wrapper.setAttribute("class", "input_wrapper");
	input_container.appendChild(input_wrapper);

	// Input
	var input = document.createElement("input");
	input.setAttribute("class", "slider");
	input.setAttribute("type", "range");
	input.setAttribute("id", id);
	input.setAttribute("min", min);
	input.setAttribute("max", max);
	input.setAttribute("step", step);
	input.setAttribute("value", value);
	input.addEventListener("click", draw);
	input_wrapper.appendChild(input);

}

function add_checkbox(parent, label_text, checked, id) {

	// Main Container
	// 		Label
	//		Wrapper
	//			Input

	// Main Container
	var input_container = document.createElement("div");
	input_container.setAttribute("class", "input_container");
	parent.appendChild(input_container);

	// Input Label
	var input_label = document.createElement("div");
	input_label.setAttribute("class", "input_label");
	input_label.innerHTML = label_text;
	input_container.appendChild(input_label);

	// Input Wrapper
	var input_wrapper = document.createElement("div");
	input_wrapper.setAttribute("class", "input_wrapper");
	input_container.appendChild(input_wrapper);

	// Input
	var input = document.createElement("input");
	input.setAttribute("class", "checkbox");
	input.setAttribute("type", "checkbox");
	input.setAttribute("id", id);
	input.addEventListener("click", draw);
	input.checked = checked;
	input_wrapper.appendChild(input);

}

function add_button(parent, label_text, action) {

	// Main Container
	//		Button

	// Main Container
	var input_container = document.createElement("div");
	input_container.setAttribute("class", "input_container");
	parent.appendChild(input_container);

	// Button
	var button = document.createElement("button");
	//button.setAttribute("class", "input_wrapper");
	button.setAttribute("type", "button")
	button.innerHTML = label_text
	button.addEventListener("click", action);
	input_container.appendChild(button);
}

function add_dropdown(parent, label_text, list, id) {

	// Main Container
	// 		Label
	//		Wrapper
	//			Select
	//				Option
	//				Option, ...

	// Main Container
	var input_container = document.createElement("div");
	input_container.setAttribute("class", "input_container");
	parent.appendChild(input_container);

	// Input Label
	var input_label = document.createElement("div");
	input_label.setAttribute("class", "input_label");
	input_label.innerHTML = label_text;
	input_container.appendChild(input_label);

	// Input Wrapper
	var input_wrapper = document.createElement("div");
	input_wrapper.setAttribute("class", "input_wrapper");
	input_container.appendChild(input_wrapper);

	// Select
	var input = document.createElement("select");
	input.setAttribute("id", id);


	// Options
	for (var i = 0; i < list.length; i++) {
		var option = document.createElement("option");
		option.value = list[i].value;
		option.innerHTML = list[i].label;
		input.appendChild(option);
	}

	input.addEventListener("click", draw);
	input_wrapper.appendChild(input);

}


function setup() {
	// svg elements must be added as ElementNS, not Element
	var ns_string = "http://www.w3.org/2000/svg";

	// An svg element with the id 'svg_artbox' must be added to the page calling this script
	// A div element with the id 'control_bar' must be added to the page calling this script
	var svg = document.getElementById("svg_artbox");
	var controller = document.getElementById("control_bar");
	if (!svg) {
		console.log("svg element not found")
		return;
	}
	if (!controller) {
		console.log("control bar element not found")
		return;
	}

	// Add background
	var bkg = document.createElementNS(ns_string, "rect");
	bkg.setAttribute("width", "100%");
	bkg.setAttribute("height", "100%");
	bkg.setAttribute("fill", "white");
	svg.appendChild(bkg);

	// Add group for main drawing area
	var drawing_group = document.createElementNS(ns_string, "g");
	drawing_group.setAttribute("id", "drawing_group");
	svg.appendChild(drawing_group);

	// List of options to add to dropdown box in controls
	var shading_list = [];
	shading_list.push({value: "00", label: "edge: top"});
	shading_list.push({value: "01", label: "edge: right"});
	shading_list.push({value: "02", label: "edge: bottom"});
	shading_list.push({value: "03", label: "edge: left"});
	shading_list.push({value: "10", label: "corner: top left"});
	shading_list.push({value: "11", label: "corner: top right"});
	shading_list.push({value: "12", label: "corner: bottom right"});
	shading_list.push({value: "13", label: "corner: bottom left"});
	shading_list.push({value: "20", label: "none: vertical"});
	shading_list.push({value: "21", label: "none: horizontal"});

	// Add controls
	add_checkbox(controller, "use color?", false, "color_check");
	add_slider(controller, "pencil color", 0, 255, 1, 0, "color_slider");
	add_slider(controller, "drawing sketchiness", 0, 1, 0.1, 0.5, "sketch_slider");
	add_slider(controller, "pencil hardness", 0, 1, 0.1, 0.5, "hardness_slider");
	add_slider(controller, "pencil sharpness", 0, 1, 0.1, 0.5, "sharpness_slider");
	add_slider(controller, "drawing pressure", 0, 1, 0.1, 0.5, "pressure_slider");
	add_slider(controller, "fill lightness", 0.25, 0.75, 0.01, 0.5, "fill_slider");
	add_dropdown(controller, "lighting direction", shading_list, "shading_choice");
	add_button(controller, "redraw", draw);
	add_button(controller, "reset", reset);

	draw();
}

setup();
