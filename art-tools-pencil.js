"use strict";

/* ***************************************

Art Tools

Copyright (C) 2018 Ascot Apricot
https://gitlab.com/ascotapricot
ascotapricot@gmail.com

This file is a part of Art Tools.

Art Tools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Art Tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Art Tools.  If not, see <https://www.gnu.org/licenses/>

*************************************** */


/* exported AT_Pencil */

/***
*
*
* Drawing Tools
* Pencil
*
*
***/

// Pencil object

function AT_Pencil(parent, hardness, sharpness, hue = -2) {
	this.parent = parent;
	this.hardness = hardness;
	this.sharpness = sharpness;
	this.hue = hue;
}

// Returns stroke color based on pencil hardness
AT_Pencil.prototype.stroke_color = function() {
	var l;
	if (this.hue >= 0) {
		l = this.hardness * 15 + 15;
		return `hsl(${this.hue}, 100%, ${l}%)`;
	} else if (this.hue == -1) {
		return "hsl(0, 0%, 100%)";
	} else {
		l = this.hardness * 36 + 8;
		return `hsl(0, 0%, ${l}%)`;
	}
};

// Returns stroke width based on pencil sharpness
AT_Pencil.prototype.stroke_width = function() {
	return 1.5 - this.sharpness;
};


/***
*
*
* Pencil Methods
*
*
***/


// Pencil line method

AT_Pencil.prototype.line_straight = function(line, sketchiness, pressure, solid, parent = this.parent ) {

	this.parent = parent;

	// add svg group to parent element
	var ns_string = "http://www.w3.org/2000/svg";
	var group = document.createElementNS(ns_string, "g");
	this.parent.appendChild(group);

	// set line style options to the group of segments
	var stroke_color = this.stroke_color();
	var stroke_width = this.stroke_width() ;
	group.setAttribute("stroke-opacity", pressure);
	group.setAttribute("stroke", stroke_color);
	group.setAttribute("stroke-width", stroke_width);
	group.classList.add("line-style");

	// find number of segments to draw
	var seg_length = 20 + sketchiness; // length of a single pencil stroke
	var weight = 1*sketchiness; // coverage of original line
	var seg_qty = weight * line.length() / seg_length;

	// angle and span of a single segment
	var line_angle = Math.atan(line.y_dist() / line.x_dist() );
	var seg_dist_x = seg_length * Math.cos(line_angle);
	var seg_dist_y = seg_length * Math.sin(line_angle);

	// draw solid baseline if solid is true
	if (solid == true) {
		var line_base = document.createElementNS(ns_string, "line");
		line_base.setAttribute("x1", line.a.x);
		line_base.setAttribute("x2", line.b.x);
		line_base.setAttribute("y1", line.a.y);
		line_base.setAttribute("y2", line.b.y);
		group.appendChild(line_base);
	}

	// draw little sketchy pencil marks
	var jitter = 1*sketchiness;
	for (var i = 0; i < seg_qty; i++) {

		var seg = document.createElementNS(ns_string, "line");
		var segment = new AT_Line();

		// vertical line
		if (line.a.x == line.b.x) {
			segment.a.x = line.a.x + Math.random()*jitter - jitter*0.5;
			segment.b.x = line.a.x + Math.random()*jitter - jitter*0.5;
			if (line.a.y < line.b.y ) {
				segment.a.y = line.a.y + Math.random()*(line.length() - seg_length);
			} else {
				segment.a.y = line.b.y + Math.random()*(line.length() - seg_length);
			}
			segment.b.y = segment.a.y + seg_dist_y + Math.random()*jitter - jitter*0.5;
		}
		// horizontal line
		else if (line.a.y == line.b.y) {
			segment.a.x = line.a.x + Math.random()*(line.length() - seg_length);
			segment.b.x = segment.a.x + seg_dist_x + Math.random()*jitter;
			segment.a.y = line.a.y + Math.random()*jitter - jitter*0.5;
			segment.b.y = line.a.y + Math.random()*jitter - jitter*0.5;
		}
		// all other diagonal lines
		else {
			segment.a.x = line.a.x + Math.random()*(line.x_dist() - seg_dist_x);
			segment.b.x = segment.a.x + seg_dist_x + Math.random()*jitter - jitter*0.5;
			segment.a.y = line.slope() * segment.a.x + line.y_intercept() + Math.random()*jitter - jitter*0.5;
			segment.b.y = segment.a.y + seg_dist_y + Math.random()*jitter - jitter*0.5;
		}

		seg.setAttribute("x1", segment.a.x);
		seg.setAttribute("x2", segment.b.x);
		seg.setAttribute("y1", segment.a.y);
		seg.setAttribute("y2", segment.b.y);
		group.appendChild(seg);
	}

	return group;
};



// Pencil hatching rectangle method

AT_Pencil.prototype.hatching_rectangle = function(rect, shading_method, shading_direction, brightness, sketchiness, pressure, parent = this.parent ) {

	this.parent = parent;


	// add svg group to parent element
	var ns_string = "http://www.w3.org/2000/svg";
	var group = document.createElementNS(ns_string, "g");
	this.parent.appendChild(group);

	var jitter = sketchiness * 1;

	// set line style options to the group of segments
	var stroke_color = this.stroke_color();
	var stroke_width = this.stroke_width() ;
	group.setAttribute("stroke", stroke_color);
	group.setAttribute("stroke-width", stroke_width);
	group.classList.add("line-style");


	function horizontal(exp, rev1, rev2, eq) {
		// find number of segments to draw and spacing
		var line_qty = 0.2 * rect.height / brightness;
		var a = rect.height / (line_qty * line_qty);
		var b = rect.height / (line_qty);

		for (var i = 0; i < line_qty; i++) {

			var seg = document.createElementNS(ns_string, "line");
			var segment = new AT_Line();

			if (rev1) {
				segment.a.x = rect.topright.x + jitter*Math.random() - 0.5*jitter;
				segment.b.x = rect.topright.x - rect.width*0.25 - Math.random()*rect.width*0.75;
			} else {
				segment.a.x = rect.topleft.x - jitter*Math.random() + 0.5*jitter;
				if (eq) {
					segment.b.x = rect.topright.x + jitter*Math.random() + 0.5*jitter;
				} else {
					segment.b.x = rect.topleft.x + rect.width*0.25 + Math.random()*rect.width*0.75;
				}

			}

			if (exp) {
				if (rev2) {
					segment.a.y = rect.bottomleft.y - a * Math.pow((i - line_qty),2) + jitter*Math.random();
				} else {
					segment.a.y = a * Math.pow((i - line_qty),2) + rect.topleft.y + jitter*Math.random();
				}
			} else {
				segment.a.y = rect.topleft.y + i * b + jitter*Math.random();
			}

			segment.b.y = segment.a.y + jitter*Math.random();

			seg.setAttribute("x1", segment.a.x);
			seg.setAttribute("x2", segment.b.x);
			seg.setAttribute("y1", segment.a.y);
			seg.setAttribute("y2", segment.b.y);

			seg.setAttribute("stroke-opacity", pressure);
			group.appendChild(seg);
		}
	}

	function vertical(exp, rev1, rev2, eq) {
		// find number of segments to draw and spacing
		var line_qty = 0.2 * rect.width / brightness;
		var a = rect.width / (line_qty * line_qty);
		var b = rect.width / (line_qty);

		for (var i = 0; i < line_qty; i++) {

			var seg = document.createElementNS(ns_string, "line");
			var segment = new AT_Line();

			if (rev1) {
				segment.a.y = rect.bottomleft.y + jitter*Math.random() - 0.5*jitter;
				segment.b.y = rect.bottomleft.y - rect.height*0.25 - Math.random()*rect.height*0.75;
			} else {
				segment.a.y = rect.topleft.y - jitter*Math.random() + 0.5*jitter ;
				if (eq) {
					segment.b.y = rect.bottomleft.y - jitter*Math.random() + 0.5*jitter;
				} else {
					segment.b.y = rect.topleft.y + rect.height*0.25 + Math.random()*rect.height*0.75;
				}

			}
			if (exp) {
				if (rev2) {
					segment.a.x = rect.topright.x - a * Math.pow((i - line_qty), 2) + jitter*Math.random();
				} else {
					segment.a.x = a * Math.pow((i - line_qty), 2) + rect.topleft.x + jitter*Math.random();
				}
			} else {
				segment.a.x = rect.topleft.x + i * b + jitter*Math.random();
			}

			segment.b.x = segment.a.x + jitter*Math.random();

			seg.setAttribute("x1", segment.a.x);
			seg.setAttribute("x2", segment.b.x);
			seg.setAttribute("y1", segment.a.y);
			seg.setAttribute("y2", segment.b.y);

			seg.setAttribute("stroke-opacity", pressure);
			group.appendChild(seg);
		}
	}

	if (shading_method == 0) {
		if (shading_direction == 0) {
			vertical(false, false, false, false);
		} else if (shading_direction == 1) {
			horizontal(false, true, false, false);
		} else if (shading_direction == 2) {
			vertical(false, true, false, false);
		} else {
			horizontal(false, false, false, false);
		}
	}
	else if (shading_method == 1) {
		if (shading_direction == 0) {
			vertical(true, false, false, false);
			horizontal(true, false, false, false);
		} else if (shading_direction == 1) {
			vertical(true, false, true, false);
			horizontal(true, true, false, false);
		} else if (shading_direction == 2) {
			vertical(true, true, true, false);
			horizontal(true, true, true, false);
		} else {
			vertical(true, true, false, false);
			horizontal(true, false, true, false);
		}
	} else {
		if (shading_direction == 0) {
			vertical(false, false, false, true);
		} else {
			horizontal(false, false, false, true);
		}
	}

	return group;
};

// Pencil crosshatching rectangle method

AT_Pencil.prototype.crosshatching_rectangle = function(rect, shading_method, shading_direction, brightness, sketchiness, pressure, parent = this.parent ) {

	this.parent = parent;


	// add svg group to parent element
	var ns_string = "http://www.w3.org/2000/svg";
	var group = document.createElementNS(ns_string, "g");
	this.parent.appendChild(group);

	var jitter = sketchiness * 1;

	// set line style options to the group of segments
	var stroke_color = this.stroke_color();
	var stroke_width = this.stroke_width() ;
	group.setAttribute("stroke", stroke_color);
	group.setAttribute("stroke-width", stroke_width);
	group.classList.add("line-style");


	function horizontal(exp, reverse) {

		// find number of segments to draw
		var line_qty = 0.1 * rect.height / brightness;
		var a = rect.height / (line_qty * line_qty);
		var b = rect.height/ (line_qty);

		for (var i = 0; i < line_qty; i++) {

			var seg = document.createElementNS(ns_string, "line");
			var segment = new AT_Line();

			segment.a.x = rect.topleft.x - jitter*Math.random() + 0.5*jitter;
			segment.b.x = rect.topright.x + jitter*Math.random();

			if (exp) {
				if (reverse) {
					segment.a.y = rect.bottomleft.y - a * Math.pow((i - line_qty),2) + jitter*Math.random();
				} else {
					segment.a.y = a * Math.pow((i - line_qty),2) + rect.topleft.y + jitter*Math.random();
				}
			} else {
				segment.a.y = rect.topleft.y + i * b + jitter*Math.random();
			}

			segment.b.y = segment.a.y + jitter*Math.random();

			seg.setAttribute("x1", segment.a.x);
			seg.setAttribute("x2", segment.b.x);
			seg.setAttribute("y1", segment.a.y);
			seg.setAttribute("y2", segment.b.y);

			seg.setAttribute("stroke-opacity", pressure);
			group.appendChild(seg);
		}
	}

	function vertical(exp, reverse) {

		// find number of segments to draw
		var line_qty = 0.1 * rect.width / brightness;
		var a = rect.width / (line_qty * line_qty);
		var b = rect.width/ (line_qty);

		for (var i = 0; i < line_qty; i++) {

			var seg = document.createElementNS(ns_string, "line");
			var segment = new AT_Line();

			segment.a.y = rect.topleft.y - jitter*Math.random() + 0.5*jitter ;
			segment.b.y = rect.bottomleft.y + jitter*Math.random() - 0.5*jitter;

			if (exp) {
				if (reverse) {
					segment.a.x = rect.topright.x - a * Math.pow((i - line_qty), 2) + jitter*Math.random();
				} else {
					segment.a.x = a * Math.pow((i - line_qty), 2) + rect.topleft.x + jitter*Math.random();
				}
			} else {
				segment.a.x = rect.topleft.x + i * b + jitter*Math.random();
			}
			segment.b.x = segment.a.x + jitter*Math.random();

			seg.setAttribute("x1", segment.a.x);
			seg.setAttribute("x2", segment.b.x);
			seg.setAttribute("y1", segment.a.y);
			seg.setAttribute("y2", segment.b.y);

			seg.setAttribute("stroke-opacity", pressure);
			group.appendChild(seg);
		}
	}

	if (shading_method == 0) {
		if (shading_direction == 0) {
			horizontal(true, false);
			vertical(false, false);
		} else if (shading_direction == 1) {
			horizontal(false, false);
			vertical(true, true);
		} else if (shading_direction == 2) {
			horizontal(true, true);
			vertical(false, false);
		} else {
			horizontal(false, false);
			vertical(true, false);
		}
	} else if (shading_method == 1) {
		if (shading_direction == 0 || shading_direction == 1) {
			horizontal(true, false);
		} else {
			horizontal(true, true);
		}
		if (shading_direction == 0 || shading_direction == 3) {
			vertical(true, false);
		} else {
			vertical(true, true);
		}
	} else {
		vertical(false, false);
		horizontal(false, false);
	}
	return group;
};

// Pencil stipple rectangle method

AT_Pencil.prototype.stipple_rectangle =  function(rect, shading_method, shading_direction, brightness, sketchiness, pressure, parent = this.parent ) {

	this.parent = parent;


	// add svg group to parent element
	var ns_string = "http://www.w3.org/2000/svg";
	var group = document.createElementNS(ns_string, "g");
	this.parent.appendChild(group);

	var jitter = sketchiness * 0.5;

	// set line style options to the group of segments
	var stroke_color = this.stroke_color();
	var stroke_width = this.stroke_width() ;
	group.setAttribute("stroke", stroke_color);
	group.setAttribute("stroke-width", stroke_width);
	group.setAttribute("stroke-opacity", pressure);
	group.classList.add("line-style");

	// find number of segments to draw
	var line_qty = 2 * (rect.bottomleft.y - rect.topleft.y) / brightness;
	var a = (rect.topright.x - rect.topleft.x) / (line_qty * line_qty);
	var b = (rect.topright.x - rect.topleft.x) / (line_qty);

	function horizontal(exp, reverse) {
		for (var i = 0; i < line_qty; i++) {

			var seg = document.createElementNS(ns_string, "line");
			var segment = new AT_Line();

			segment.a.x = rect.topleft.x + Math.random() * rect.width;
			segment.b.x = segment.a.x + 1;

			if (exp) {
				if (reverse) {
					segment.a.y = rect.bottomleft.y - a * Math.pow((i - line_qty),2) + jitter*Math.random();
				} else {
					segment.a.y = a * Math.pow((i - line_qty),2) + rect.topleft.y + jitter*Math.random();
				}
			} else {
				segment.a.y = rect.topleft.y + i * b + jitter*Math.random();
			}

			segment.b.y = segment.a.y + jitter*Math.random();

			seg.setAttribute("x1", segment.a.x);
			seg.setAttribute("x2", segment.b.x);
			seg.setAttribute("y1", segment.a.y);
			seg.setAttribute("y2", segment.b.y);
			group.appendChild(seg);
		}
	}

	function vertical(exp, reverse) {
		for (var i = 0; i < line_qty; i++) {

			var seg = document.createElementNS(ns_string, "line");
			var segment = new AT_Line();

			segment.a.y = rect.topleft.y + Math.random() * rect.height;
			segment.b.y = segment.a.y + 1;

			if (exp) {
				if (reverse) {
					segment.a.x = rect.topright.x - a * Math.pow((i - line_qty), 2) + jitter*Math.random();
				} else {
					segment.a.x = a * Math.pow((i - line_qty), 2) + rect.topleft.x + jitter*Math.random();
				}
			} else {
				segment.a.x = rect.topleft.x + i * b + jitter*Math.random();
			}
			segment.b.x = segment.a.x + jitter*Math.random();

			seg.setAttribute("x1", segment.a.x);
			seg.setAttribute("x2", segment.b.x);
			seg.setAttribute("y1", segment.a.y);
			seg.setAttribute("y2", segment.b.y);
			group.appendChild(seg);
		}
	}

	if (shading_method == 1) {
		if (shading_direction == 0 || shading_direction == 1) {
			horizontal(true, false);
		} else {
			horizontal(true, true);
		}
		if (shading_direction == 0 || shading_direction == 3) {
			vertical(true, false);
		} else {
			vertical(true, true);
		}
	} else {
		if (shading_direction == 0) {
			horizontal(true, false);
			vertical(false, false);
		} else if (shading_direction == 1) {
			horizontal(false, false);
			vertical(true, true);
		} else if (shading_direction == 2) {
			horizontal(true, true);
			vertical(false, false);
		} else {
			horizontal(false, false);
			vertical(true, false);
		}
	}
	return group;
};



// Pencil line circle method

AT_Pencil.prototype.line_circle =  function(circle, quadrants = [1,2,3,4] , sketchiness, pressure, parent = this.parent ) {

	this.parent = parent;

	// add svg group to parent element
	var ns_string = "http://www.w3.org/2000/svg";
	var group = document.createElementNS(ns_string, "g");
	this.parent.appendChild(group);

	// set line style options to the group of segments
	var stroke_color = this.stroke_color();
	var stroke_width = this.stroke_width() ;
	group.setAttribute("stroke-opacity", pressure);
	group.setAttribute("stroke", stroke_color);
	group.setAttribute("stroke-width", stroke_width);
	group.classList.add("line-style");

	// find number of segments to draw
	var seg_length = 20 + sketchiness; // length of a single pencil stroke
	var weight = 1*sketchiness; // coverage of original line
	var quadrant_length = circle.circumference() / 4;
	var seg_qty = 1.5 * weight * quadrant_length / seg_length;

	// TODO draw solid baseline if solid is true

	// draw little sketchy pencil marks
	var jitter = 1*sketchiness;

	for (var j = 0; j < quadrants.length; j++) {

		var quad = quadrants[j];
		for (var i = 0; i < seg_qty; i++) {

			// angle and span of a single segment
			var seg_angle = Math.random() * Math.PI / 2;
			var seg = document.createElementNS(ns_string, "line");
			var segment = new AT_Line();


			var seg_dist_x, seg_dist_y, seg_x, seg_y;

			if (quad == 4 || quad == 1) {
				seg_dist_x = seg_length * Math.sin(seg_angle) / 2;
				seg_x = circle.center.x + circle.radius * Math.cos(seg_angle);
				segment.a.x = seg_x + seg_dist_x + jitter*Math.random() - jitter*0.5;
				segment.b.x = seg_x - seg_dist_x + jitter*Math.random() - jitter*0.5;
			} else {
				seg_dist_x = seg_length * Math.sin(seg_angle + Math.PI ) / 2;
				seg_x = circle.center.x - circle.radius * Math.cos(seg_angle);
				segment.a.x = seg_x + seg_dist_x + jitter*Math.random() - jitter*0.5;
				segment.b.x = seg_x - seg_dist_x + jitter*Math.random() - jitter*0.5;
			}

			if (quad == 1 || quad == 2) {
				seg_dist_y = seg_length * Math.cos(seg_angle) / 2;
				seg_y = circle.center.y - circle.radius * Math.sin(seg_angle);
				segment.a.y = seg_y + seg_dist_y + jitter*Math.random() - jitter*0.5;
				segment.b.y = seg_y - seg_dist_y + jitter*Math.random() - jitter*0.5;
			} else {
				seg_dist_y = seg_length * Math.cos(seg_angle + Math.PI ) / 2;
				seg_y = circle.center.y + circle.radius * Math.sin(seg_angle);
				segment.a.y = seg_y + seg_dist_y + jitter*Math.random() - jitter*0.5;
				segment.b.y = seg_y - seg_dist_y + jitter*Math.random() - jitter*0.5;
			}

			seg.setAttribute("x1", segment.a.x);
			seg.setAttribute("x2", segment.b.x);
			seg.setAttribute("y1", segment.a.y);
			seg.setAttribute("y2", segment.b.y);
			group.appendChild(seg);
		}
	}

	return group;
};


// Pencil hatching circle method

// Circle should be a AT_circle object
// shading_direction should be an index in the range of 0-3, referring to one of the edges
// Sketchiness represents how messy or neat the drawing is, and should be a floating point
//		number between 0 and 10, roughly used as deviation away from the nominal line object
// Brightness represents the level of shading, and should be a float between 0 and 1 where
//		0 is the darkest and 1 is the lightest. Reasonable values are probably between


AT_Pencil.prototype.hatching_circle = function(circle, shading_method, shading_direction, brightness, sketchiness, pressure, parent = this.parent ) {

	this.parent = parent;


	// add svg group to parent element
	var ns_string = "http://www.w3.org/2000/svg";
	var group = document.createElementNS(ns_string, "g");
	this.parent.appendChild(group);

	var jitter = sketchiness * 1;

	// set line style options to the group of segments
	var stroke_color = this.stroke_color();
	var stroke_width = this.stroke_width() ;
	group.setAttribute("stroke", stroke_color);
	group.setAttribute("stroke-width", stroke_width);
	group.classList.add("line-style");

	// find number of segments to draw
	var line_qty = 0.3 * circle.diameter / brightness;
	var spacing = circle.diameter / line_qty;

	function horizontal(rev, eq = false) {
		for (var i = 0; i < line_qty; i++) {

			var seg = document.createElementNS(ns_string, "line");
			var segment = new AT_Line();

			var segment_x = spacing * i;

			var circle_height;
			if (eq) {
				circle_height = circle.height_at_x(segment_x);
				segment.a.x = circle.center.x - circle_height / 2 ;
				segment.b.x = circle.center.x + circle_height / 2 ;
			}
			else if (rev) {
				circle_height = circle.height_at_x(segment_x);
				segment.a.x = circle.center.x - circle_height / 2 ;
				segment.b.x = segment.a.x + circle_height*0.5 + circle_height*Math.random()*.5 ;
			} else {
				circle_height = circle.height_at_x(segment_x);
				segment.a.x = circle.center.x + circle_height / 2 ;
				segment.b.x = segment.a.x - circle_height*0.5 - circle_height*Math.random()*.5 ;

			}

			segment.a.y = circle.center.y - circle.radius + segment_x + jitter*Math.random() - jitter*0.5;
			segment.b.y = circle.center.y - circle.radius + segment_x + jitter*Math.random() - jitter*0.5;

			seg.setAttribute("x1", segment.a.x);
			seg.setAttribute("x2", segment.b.x);
			seg.setAttribute("y1", segment.a.y);
			seg.setAttribute("y2", segment.b.y);

			seg.setAttribute("stroke-opacity", pressure);
			group.appendChild(seg);
		}
	}

	function vertical(rev, eq = false) {
		for (var i = 0; i < line_qty; i++) {

			var seg = document.createElementNS(ns_string, "line");
			var segment = new AT_Line();

			var segment_x = spacing * i;

			var circle_height;
			if (eq) {
				circle_height = circle.height_at_x(segment_x);
				segment.a.y = circle.center.y + circle_height / 2 ;
				segment.b.y = circle.center.y - circle_height / 2 ;
			}
			else if (rev) {
				circle_height = circle.height_at_x(segment_x);
				segment.a.y = circle.center.y + circle_height / 2 ;
				segment.b.y = segment.a.y - circle_height*0.5 - circle_height*Math.random()*.5 ;
			} else {
				circle_height = circle.height_at_x(segment_x);
				segment.a.y = circle.center.y - circle_height / 2 ;
				segment.b.y = segment.a.y + circle_height*0.5 + circle_height*Math.random()*.5 ;
			}

			segment.a.x = circle.center.x - circle.radius + segment_x + jitter*Math.random() - jitter*0.5;
			segment.b.x = circle.center.x - circle.radius + segment_x + jitter*Math.random() - jitter*0.5;

			seg.setAttribute("x1", segment.a.x);
			seg.setAttribute("x2", segment.b.x);
			seg.setAttribute("y1", segment.a.y);
			seg.setAttribute("y2", segment.b.y);

			seg.setAttribute("stroke-opacity", pressure);
			group.appendChild(seg);
		}
	}

	if (shading_method == 2) {
		if (shading_direction == 0) {
			vertical(false, true);
		} else {
			horizontal(false, true);
		}
	} else if (shading_method == 1) {
		if (shading_direction == 0 || shading_direction == 1) {
			vertical(false);
		} else {
			vertical(true);
		}
		if (shading_direction == 0 || shading_direction == 3) {
			horizontal(true);
		} else {
			horizontal(false);
		}
	} else{
		if (shading_direction == 0) {
			vertical(false);
		} else if (shading_direction == 1) {
			horizontal(false);
		} else if (shading_direction == 2) {
			vertical(true);
		} else {
			horizontal(true);
		}
	}


	return group;
};
