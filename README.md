# Art Tools

* [Overview](#overview)
* [Outlook](#outlook)
* [Quick Start](#quick-start)
* [Detailed Usage](#detailed-usage)
	+ [Class: AT_Point](#class-at_point)
	+ [Class: AT_Line](#class-at_line)
	+ [Class: AT_Polyline](#class-at_polyline)
	+ [Class: AT_Rect](#class-at_rect)
	+ [Class: AT_Circle](#class-at_circle)
	+ [Class: AT_Pencil](#class-at_pencil)
	+ [Method: AT_Pencil.line_straight](#method-at_pencilline_straight)
	+ [Method: AT_Pencil.line_circle](#method-at_pencilline_circle)
	+ [Method: AT_Pencil.hatching_rectangle](#method-at_pencilhatching_rectangle)
	+ [Method: AT_Pencil.hatching_crossrectangle](#method-at_pencilcrosshatching_rectangle)
	+ [Method: AT_Pencil.stipple_rectangle](#method-at_pencilstipple_rectangle)
	+ [Method: AT_Pencil.hatching_circle](#method-at_pencilhatching_circle)
* [Copyright and License](#copyright-and-license)

# Overview

Art Tools is a library of procedural art tools. My intention was to create drawing tools that mimic the manner in which traditional art tools, like pencils, brushes, or pens, are used, in the library's algorithms, and to mimic the visual result in a vector format. The methods provided create drawings as svg objects.

The library contains classes representing an *art tool.* The properties of the class represent physical properties of the tool itself, such as the *colour* of a pencil. The class methods represent actions you could take with that tool, such as *drawing a line.* The parameters of the methods represent the manner in which you would perform these actions, such as drawing a line *with a lot of pressure.*

| Library Structure 	| Real World Structure 		| Example 1				| Example 2				|
| --- 					| --- 							| --- 					| ---						|
| Class 					| Tool 							| Pencil 				| Pencil					|
| Class Property 		| Physical Property 			| Red-coloured lead 	| 2B Graphite lead	|
| Method 				| Physical Action 			| Crosshatching 		| Stippling				|
| Method Parameter	| Manner of actions			| Messy					| Tidy					|


__Example 1__:

![example 1 image](img/ex1.jpg)

__Example 2__:

![example 2 image](img/ex2.jpg)

The library also contains primitive classes used to define the shapes you want to draw.

# Outlook

While I have started with a pencil, I intend to add more tools such as paint brushes or pastels.

# Quick start

In the demo folder, there are two examples. Art-tools-demo-simple.html has an inline script that
There is a demo that includes a set of swatches with examples of basic geometry available for use, and on page controls to see the effects of different parameters on the output.


* Create an html page that imports svg-tools.js, art-tools-primitives.js, and art-tools-pencil.js.
```html
<head>
	<script src="svg-tools.js"></script>
	<script src="art-tools-primitives.js"></script>
	<script src="art-tools-pencil.js"></script>
</head>
```
* Add an svg element to the body, and give it an ID.
```html
<body>
	<svg id="mysvg"></svg>
</body>
```
* In your script, find your svg
```js
var svg = document.getElementById("mysvg");
```
* Create a pencil object, specifying hardness, sharpness, and hue
```js
var pencil_hardness = 0.5;
var pencil_sharpness = 0.5;
var pencil_hue = -2; // 0-255 is an rgb hue, -1 is white, and -2 is graphite
var pencil = new AT_Pencil(svg, pencil_hardness, pencil_sharpness, pencil_hue);
```
* Create two points using their x, y coordinates
```js
var point_a_x = 50;
var point_a_y = 50;
var point_a = new AT_Point(point_a_x, point_a_y);
var point_b_x = 200;
var point_b_y = 200;
var point_b = new AT_Point(point_b_x, point_b_y);
```
* Create a line, using the two points you just created.
```js
var line = new AT_Line(point_a, point_b);
```
* Use a pencil method to draw the line, specifying the sketchiness and pressure, and whether or not to draw a single solid line underneath the pencil strokes.
```js
var sketchiness_level = 5;
var pressure_level = 0.5;
var solid = true;
pencil.line_straight(line, sketchiness_level, pressure_level, solid );
```
# Detailed Usage

Primitive Classes
* [Class: AT_Point](#class-at_point)
* [Class: AT_Line](#class-at_line)
* [Class: AT_Polyline](#class-at_polyline)
* [Class: AT_Rect](#class-at_rect)
* [Class: AT_Circle](#class-at_circle)

Pencil Class
* [Class: AT_Pencil](#class-at_pencil)

Line Methods
* [Method: AT_Pencil.line_straight](#method-at_pencilline_straight)
* [Method: AT_Pencil.line_circle](#method-at_pencilline_circle)

Fill Methods
* [Method: AT_Pencil.hatching_rectangle](#method-at_pencilhatching_rectangle)
* [Method: AT_Pencil.crosshatching_rectangle](#method-at_pencilcrosshatching_rectangle)
* [Method: AT_Pencil.stipple_rectangle](#method-at_pencilstipple_rectangle)
* [Method: AT_Pencil.hatching_circle](#method-at_pencilhatching_circle)

## Class: AT_Point

__Usage__
This class represents a point in the svg. Units are the current svg units (default is pixels), and the coordinate system is the current svg  coordinate system (default = X horizontal & increase moving right & origin is left edge, Y vertical & increases moving down & origin is top edge).
```js
var point = new AT_Point(x = 0, y = 0);
```
__Optional arguments:__
* X is the x coordinate of the point. Default is 0.
	- Type: number
* Y is the y coordinate of the point. Default is 0.
	- Type: number

## Class: AT_Line

__Usage__
This class represents a line segment between two points. A is always the point with the lesser X value, unless the line is vertical.
```js
var line = new AT_Line(a = new AT_Point, b = new AT_Point);
```
__Optional arguments:__
* A is one end of the line. Default is a new point.
	- Type: object, AT_Point
* B is one end of the line. Default is a new point.
	- Type: object, AT_Point

__Methods:__
* AT_Line.x_dist() returns the total span over the x-axis
* AT_Line.y_dist() returns the total span over the y-axis
* AT_Line.length() returns the length of the line segment
* AT_Line.slope() returns the slope if slope is defined
* AT_Line.y_intercept() returns the y intercept if slope is defined

## Class: AT_Polyline

__Usage__
This class represents a series of line segments, between a sequence of points.
```js
var points = [new AT_Point(), new AT_Point(), new AT_Point()];
var line = new AT_Polyline(points);
```
__Required arguments:__
* points is an array of points
	- Type: array of AT_Point objects

## Class: AT_Rect

__Usage__
This class represents a rectangle.
```js
var rectangle = new AT_Rect(topleft = new AT_Point(), width = 50, height = 50);
```
__Optional arguments:__
* Topleft is the point at the top left vertex of the rectangle
	- Type: object, AT_Point
* Width is the width of the rectangle in pixels. Default is 50.
	- Type: number
	- Range: > 0
* Height is the height of the rectangle in pixels. Default is 50.
	- Type: number
	- Range: > 0

__Properties:__
* Topright returns the point at the top right vertex of the rectangle.
* Bottomleft returns the point at the bottom left vertex of the rectangle.
* Bottomright returns the point at the bottom right vertex of the rectangle.
* Corners returns an array of the points at all of the vertices.
* Top returns a line segment on the top edge of the rectangle.
* Bottom returns a line segment on the bottom edge of the rectangle.
* Right returns a line segment on the right edge of the rectangle.
* Left returns a line segment on the left edge of the rectangle.
* Edges returns an array of all the line segments on the edges.

__Methods:__
* AT_Line.diagonal() returns the a line segment from the top left corner to the bottom right corner of the rectangle.

## Class: AT_Circle

__Usage__
This class represents a circle.
```js
var circle = new AT_Circle(center = new AT_Point(), radius = 100);
```

__Optional arguments:__
* Center is the point at the center of the circle. Default is a new point at (0,0)
	- Type: object, AT_Point
* Radius is the radius of the circle. Default is 100.
	- Type: number
	- Range: > 0

__Properties:__
* Diameter returns the diameter of the circle.

__Methods:__
* AT_Circle.circumference() returns the circumference of the circle.

## Class: AT_Pencil

__Usage__
```js
var pencil = new AT_Pencil(parent, hardness, sharpness, hue = -2);
```
__Required arguments:__
* Parent is html dom node in which to insert drawings.All drawings are inserted as child nodes, contained in a <g> element.
	- Type: object / html node, svg or g
* Hardness represents the hardness of the graphite, and should be a number between 0 and 1, where 0 is the softest (like B lead), and 1 is the hardest (like H lead). This is used to determine svg "stroke-color", affecting both hue and lightness in a hsl colour scheme.
	- Type: number
	- Range: 0 - 1 (soft to hard)
* Sharpness represents how small of a point the pencil is sharpened to, and should be a number between 0 and 1, where 1 is the sharpest and 0 is the dullest. This is used to determine svg "stroke-width", placing it between 0.5 and 1.5 pixels.
	- Type: number
	- Range: 0 - 1 (sharp - dull)

__Optional arguments:__
* Hue is the colour of the pencil lead. It can be either a colour, white, or grey.
	- Type: number
	- Range: -2 is graphite grey (default), -1 is white, and 0 - 360 is a colour in degrees, as used by HSL colour format.

## Method: AT_Pencil.line_straight

__Actions__
* Sketches a straight line composed of many very small line segments.

![example](img/method-line-straight.jpg)

__Usage__
```js
pencil.line_straight(line, sketchiness, pressure, solid, parent = this.parent);
```
__Required arguments__
* Line is the line you want to draw. It should be an AT_Line object.
	- Type: object / AT_Line
* Sketchiness is how messy or neat the individual sketch marks are. Roughly speaking, it is used as the deviation away from the nominal line you are drawing, in pixels.
	- Type: number
	- Range: 0-10 (cleanest lines - messiest lines)
* Pressure is how hard the pencil is pressed against paper. It is used for line opacity.
	- Type: number
	- Range: 0-1 (lightest - heaviest)
* Solid is whether or not to draw a single solid base line, underneath other sketch lines. In some cases, when sketchiness is set very high, the nominal line may be completely filled in.
	- Type: boolean (false = doesn't draw line, true = draws line)

__Optional arguments:__
* Parent may be used to draw in a svg node different from that of the pencil itself. This does not change the parent property of the pencil. It only temporarily draws in a different node. If you wish to permanently change canvases, change the property in the pencil class. By default, this method uses the parent of the pencil.
	- Type: object / html node, svg or g


## Method: AT_Pencil.line_circle

__Actions__
* Sketches a line around one or more quadrants of a circle. All four quadrants are shown in the picture.

![example](img/method-line-circle.jpg)
 
__Usage__
```js
pencil.line_circle(circle, quadrants = [1,2,3,4] , sketchiness, pressure, parent = this.parent );
```

__Required arguments__
* Circle is the circle you want to sketch the outline of. It should be an AT_Circle object.
	- Type: object / AT_Circle
* Sketchiness is how messy or neat the individual sketch marks are. Roughly speaking, it is used as the deviation away from the nominal line you are drawing, in pixels.
	- Type: number
	- Range: 0-10 (cleanest lines - messiest lines)
* Pressure is how hard the pencil is pressed against paper. It is used for line opacity.
	- Type: number
	- Range: 0-1 (lightest - heaviest)

__Optional arguments:__
* Quadrants is a list of which quadrants to draw. The default is all 4 quadrants, [1,2,3,4]. You must choose at least one quadrant.
	- Type: array of numbers
	- Valid Options:
		+ 1 - Top Right
		+ 2 - Top left
		+ 3 - Bottom Left
		+ 4 - Bottom Right
* Parent may be used to draw in a svg node different from that of the pencil itself. This does not change the parent property of the pencil. It only temporarily draws in a different node. If you wish to permanently change canvases, change the property in the pencil class. By default, this method uses the parent of the pencil.
	- Type: object / html node, svg or g


## Method: AT_Pencil.hatching_rectangle

__Actions__
* Fills in a rectangle using a hatching pattern.

![example](img/method-hatch-rectangle.jpg)

__Usage__
```js
pencil.hatching_rectangle(rect, shading_method, shading_direction, brightness, sketchiness, pressure, parent = this.parent );
```
__Required arguments__
* Rect is the rectangle area you want to fill. It should be an AT_Rect object.
	- Type: object / AT_Line
* Shading Method indicates what shading pattern to use. This, in combination with shading direction, determines the final manner in which the shape is shaded. See demo for usage examples. It's probably easier to see.
	- Type: number
	- Valid Options:
		+ 0 - Darkest along EDGE (single hatch)
		+ 1 - Darkest at a CORNER (double hatch)
		+ 2 - Uniform shading (no dark area)
* Shading direction
	- Type: number
	- Valid Options:
		+ 0 - Top (edge) or Top Left (corner) or Vertical (Uniform)
		+ 1 - Right (edge) or Top Right (corner) or Horizontal (Uniform)
		+ 2 - Bottom (edge) or Bottom Right (corner)
		+ 3 - Left (edge) or Bottom Left (corner)
* Brightness is how dark the space is filled in. This corresponds to the number of lines used to sketch in the area.
	- Type: number
	- Range: 0-1 (darkest - lightest), although 0.25-0.75 is probably the most reasonable range. Values close to 0 or 1 will give extreme results. Values close to 0 may be very slow to draw.
* Sketchiness is how messy or neat the individual sketch marks are. Roughly speaking, it is used as the deviation away from the nominal line you are drawing, in pixels.
	- Type: number
	- Range: 0-10 (cleanest lines - messiest lines)
* Pressure is how hard the pencil is pressed against paper. It is used for line opacity.
	- Type: number
	- Range: 0-1 (lightest - heaviest)
* Solid is whether or not to draw a single solid base line, underneath other sketch lines. In some cases, when sketchiness is set very high, the nominal line may be completely filled in.
	- Type: boolean (false = doesn't draw line, true = draws line)

__Optional arguments:__
* Parent may be used to draw in a svg node different from that of the pencil itself. This does not change the parent property of the pencil. It only temporarily draws in a different node. If you wish to permanently change canvases, change the property in the pencil class. By default, this method uses the parent of the pencil.
	- Type: object / html node, svg or g

## Method: AT_Pencil.crosshatching_rectangle

__Actions__
* Fills in a rectangle using a crosshatching pattern.

![example](img/method-crosshatch-rectangle.jpg)

__Usage__
```js
pencil.crosshatching_rectangle(rect, shading_method, shading_direction, brightness, sketchiness, pressure, parent = this.parent );
```

__Required arguments__
* Rect is the rectangle area you want to fill. It should be an AT_Rect object.
	- Type: object / AT_Line
* Shading Method indicates what shading pattern to use. This, in combination with shading direction, determines the final manner in which the shape is shaded. See demo for usage examples. It's probably easier to see.
	- Type: number
	- Valid Options:
		+ 0 - Darkest along EDGE (crosshatched, single axis shaded)
		+ 1 - Darkest at a CORNER (crosshatched, double axis shaded)
		+ 2 - Uniform shading (no dark area)
* Shading direction
	- Type: number
	- Valid Options:
		+ 0 - Top (edge) or Top Left (corner) or Vertical (Uniform)
		+ 1 - Right (edge) or Top Right (corner) or Horizontal (Uniform)
		+ 2 - Bottom (edge) or Bottom Right (corner)
		+ 3 - Left (edge) or Bottom Left (corner)
* Brightness is how dark the space is filled in. This corresponds to the number of lines used to sketch in the area.
	- Type: number
	- Range: 0-1 (darkest - lightest), although 0.25-0.75 is probably the most reasonable range. Values close to 0 or 1 will give extreme results. Values close to 0 may be very slow to draw.
* Sketchiness is how messy or neat the individual sketch marks are. Roughly speaking, it is used as the deviation away from the nominal line you are drawing, in pixels.
	- Type: number
	- Range: 0-10 (cleanest lines - messiest lines)
* Pressure is how hard the pencil is pressed against paper. It is used for line opacity.
	- Type: number
	- Range: 0-1 (lightest - heaviest)
* Solid is whether or not to draw a single solid base line, underneath other sketch lines. In some cases, when sketchiness is set very high, the nominal line may be completely filled in.
	- Type: boolean (false = doesn't draw line, true = draws line)

__Optional arguments:__
* Parent may be used to draw in a svg node different from that of the pencil itself. This does not change the parent property of the pencil. It only temporarily draws in a different node. If you wish to permanently change canvases, change the property in the pencil class. By default, this method uses the parent of the pencil.
	- Type: object / html node, svg or g

## Method: AT_Pencil.stipple_rectangle

__Actions__
* Fills in a rectangle using a stipple pattern.

![example](img/method-stipple-rectangle.jpg)

__Usage__
```js
pencil.stipple_rectangle(rect, shading_method, shading_direction, brightness, sketchiness, pressure, parent = this.parent );
```

__Required arguments__
* Rect is the rectangle area you want to fill. It should be an AT_Rect object.
	- Type: object / AT_Line
* Shading Method indicates what shading pattern to use. This, in combination with shading direction, determines the final manner in which the shape is shaded. See demo for usage examples. It's probably easier to see.
	- Type: number
	- Valid Options:
		+ 0 - Darkest along EDGE
		+ 1 - Darkest at a CORNER
		+ 2 - Uniform shading (no dark area)
* Shading direction
	- Type: number
	- Valid Options:
		+ 0 - Top (edge) or Top Left (corner) or Vertical (Uniform)
		+ 1 - Right (edge) or Top Right (corner) or Horizontal (Uniform)
		+ 2 - Bottom (edge) or Bottom Right (corner)
		+ 3 - Left (edge) or Bottom Left (corner)
* Brightness is how dark the space is filled in. This corresponds to the number of lines used to sketch in the area.
	- Type: number
	- Range: 0-1 (darkest - lightest), although 0.25-0.75 is probably the most reasonable range. Values close to 0 or 1 will give extreme results. Values close to 0 may be very slow to draw.
* Sketchiness is how messy or neat the individual sketch marks are. Roughly speaking, it is used as the deviation away from the nominal line you are drawing, in pixels.
	- Type: number
	- Range: 0-10 (cleanest lines - messiest lines)
* Pressure is how hard the pencil is pressed against paper. It is used for line opacity.
	- Type: number
	- Range: 0-1 (lightest - heaviest)
* Solid is whether or not to draw a single solid base line, underneath other sketch lines. In some cases, when sketchiness is set very high, the nominal line may be completely filled in.
	- Type: boolean (false = doesn't draw line, true = draws line)

__Optional arguments:__
* Parent may be used to draw in a svg node different from that of the pencil itself. This does not change the parent property of the pencil. It only temporarily draws in a different node. If you wish to permanently change canvases, change the property in the pencil class. By default, this method uses the parent of the pencil.
	- Type: object / html node, svg or g

## Method: AT_Pencil.hatching_circle

__Actions__
* Fills in a circle using a hatching pattern.

![example](img/method-hatch-circle.jpg)

__Usage__
```js
pencil.hatching_circle(circle, shading_method, shading_direction, brightness, sketchiness, pressure, parent = this.parent );
```

__Required arguments__
* Circle is the circle you want to sketch the outline of. It should be an AT_Circle object.
	- Type: object / AT_Circle
* Shading Method indicates what shading pattern to use. This, in combination with shading direction, determines the final manner in which the shape is shaded. See demo for usage examples. It's probably easier to see.
	- Type: number
	- Valid Options:
		+ 0 - Darkest along EDGE (crosshatched, single axis shaded)
		+ 1 - Darkest at a CORNER (crosshatched, double axis shaded)
		+ 2 - Uniform shading (no dark area)
* Shading direction
	- Type: number
	- Valid Options:
		+ 0 - Top (edge) or Top Left (corner) or Vertical (Uniform)
		+ 1 - Right (edge) or Top Right (corner) or Horizontal (Uniform)
		+ 2 - Bottom (edge) or Bottom Right (corner)
		+ 3 - Left (edge) or Bottom Left (corner)
* Brightness is how dark the space is filled in. This corresponds to the number of lines used to sketch in the area.
	- Type: number
	- Range: 0-1 (darkest - lightest), although 0.25-0.75 is probably the most reasonable range. Values close to 0 or 1 will give extreme results. Values close to 0 may be very slow to draw.
* Sketchiness is how messy or neat the individual sketch marks are. Roughly speaking, it is used as the deviation away from the nominal line you are drawing, in pixels.
	- Type: number
	- Range: 0-10 (cleanest lines - messiest lines)
* Pressure is how hard the pencil is pressed against paper. It is used for line opacity.
	- Type: number
	- Range: 0-1 (lightest - heaviest)
* Solid is whether or not to draw a single solid base line, underneath other sketch lines. In some cases, when sketchiness is set very high, the nominal line may be completely filled in.
	- Type: boolean (false = doesn't draw line, true = draws line)

__Optional arguments:__
* Parent may be used to draw in a svg node different from that of the pencil itself. This does not change the parent property of the pencil. It only temporarily draws in a different node. If you wish to permanently change canvases, change the property in the pencil class. By default, this method uses the parent of the pencil.
	- Type: object / html node, svg or g

# Copyright and License
Code, documentation, and examples copyright 2018-2019 Ascot Apricot. Licensed with GNU GPLv3. See COPYING for more details. Contact me at ascotapricot at gmail dot com.
