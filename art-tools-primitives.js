"use strict";

/* ***************************************

Art Tools

Copyright (C) 2018 Ascot Apricot
https://gitlab.com/ascotapricot
ascotapricot@gmail.com

This file is a part of Art Tools.

Art Tools is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Art Tools is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Art Tools.  If not, see <https://www.gnu.org/licenses/>

*************************************** */

/* exported AT_Point, AT_Line, AT_Rect, AT_Circle, AT_Polyline */

/***
*
*
* Primitive data types
*
*
***/


// Point
// x and y are any coordinates found in the svg
// default values are at origin

function AT_Point(x = 0, y = 0) {
	this.x = x;
	this.y = y;
}



// Line
// a and b should be AT_Point objects
// if no points are given, then new ones are made

function AT_Line(a = new AT_Point, b = new AT_Point) {
	this.a = a;
	this.b = b;
	if (a.x > b.x) {
		this.a = b;
		this.b = a;
	}
}

// Returns total span on x axis
AT_Line.prototype.x_dist = function() {
	return this.b.x - this.a.x;
};

// Returns total span on y axis
AT_Line.prototype.y_dist = function() {
	return this.b.y - this.a.y;
};

// Returns length of line
AT_Line.prototype.length = function() {
	return Math.sqrt(Math.pow(this.x_dist(), 2) + Math.pow(this.y_dist(), 2));
};

// Returns slope of line if slope is defined
AT_Line.prototype.slope = function() {
	if (this.a.x !== this.b.x) {
		return this.y_dist() / this.x_dist();
	}
};

// Returns y intercept if slope is defined
AT_Line.prototype.y_intercept = function() {
	var slope = this.slope();
	if (slope) {
		return this.a.y - slope * this.a.x;
	}
};


// Polyline
// points should be an array of AT_Point objects,
//		in order of sequence along the polyline

function AT_Polyline(points) {
	this.points = points;
	this.segments = [];
	var i;
	for (i = 0; i < points.length - 1; i++) {
		this.segments.push(new AT_Line(this.points[i], this.points[i+1]));
	}
}


// Rectangle
// topleft is a point at the top left corner of the rectangle
// width, height span right and down from x, y, and must be positive
// 		default is 50 and 50
// topleft, topright, bottomleft, and bottomright are points on the corner of the rectangle
// top, bottom, left, and right are the lines between these points

function AT_Rect(topleft = new AT_Point(), width = 50, height = 50) {
	this.width = width;
	this.height = height;

	this.topleft = topleft;
	this.topright = new AT_Point(this.topleft.x + this.width, this.topleft.y);
	this.bottomleft = new AT_Point(this.topleft.x, this.topleft.y + this.height);
	this.bottomright = new AT_Point(this.topleft.x + this.width, this.topleft.y + this.height);
	this.corners = [this.topleft, this.topright, this.bottomright, this.bottomleft];

	this.top = new AT_Line(this.topleft, this.topright);
	this.bottom = new AT_Line(this.bottomleft, this.bottomright);
	this.left = new AT_Line(this.topleft, this.bottomleft);
	this.right = new AT_Line(this.topright, this.bottomright);
	this.edges = [this.top, this.right, this.bottom, this.left];
}

AT_Rect.prototype.diagonal = function() {
	let diagonal = new AT_Line;
	diagonal.a = this.top.a;
	diagonal.b = this.bottom.b;
	return diagonal;
};




// Circle
// center is a point at the center. Default is at 0,0
// radius is the radius, in pixels. Default is 100.

function AT_Circle(center = new AT_Point(), radius = 100) {
	this.center = center;
	this.radius = radius;

	this.diameter = radius * 2;
}


AT_Circle.prototype.circumference = function() {
	return Math.PI * this.diameter;
};

// height of the circle at a X
AT_Circle.prototype.height_at_x = function(x) {
	var a = this.radius - x;
	var h = 2 * Math.sqrt((this.radius * this.radius) - (a * a));
	return h;
};
